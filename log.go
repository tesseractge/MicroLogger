package log

import (
	"github.com/fatih/color"
	l "log"
)

type LoggerLevel int

const (
	DEBUG LoggerLevel = iota
	INFO
	NOTICE
	WARN
	ERR
	CRIT
	ALERT
	EMERG
	EXTENSION
)

var CriticalColor = color.New(color.BgHiRed, color.FgHiWhite)
var AlertColor = color.New(color.BgCyan, color.FgHiWhite)
var EmergencyColor = color.New(color.BgHiMagenta, color.FgHiWhite)

func Log(level LoggerLevel, log string) {
	switch level {
	case DEBUG:
		l.Printf("[ %s ] %s\n", color.BlueString("DBUG"), log)
		break
	case INFO:
		l.Printf("[ INFO ] %s\n", log)
		break
	case NOTICE:
		l.Printf("[ %s ] %s\n", color.HiGreenString("NOTE"), log)
		break
	case WARN:
		l.Printf("[ %s ] %s\n", color.HiYellowString("WARN"), log)
		break
	case ERR:
		l.Printf("[ %s ] %s\n", color.HiRedString("FAIL"), log)
		break
	case CRIT:
		l.Fatalf("[ %s ] %s\n", CriticalColor.Sprint("CRIT"), log)
		break
	case ALERT:
		l.Fatalf("[ %s ] %s\n", AlertColor.Sprint("ALRT"), log)
		break
	case EMERG:
		l.Fatalf("[ %s ] %s\n", EmergencyColor.Sprint("EMRG"), log)
		break
	case EXTENSION:
		l.Println("          ->", log)
		break
	default:
		l.Printf("[%s] Parameter level is not of type LoggerLevel!\n", color.HiRedString("EROR"))
		break
	}
}
